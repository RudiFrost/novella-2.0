create table achievements_to_users
(
    id int generated always as identity primary key,
    user_id int references users (id) not null,
    achievement_id int references achievements (id) not null,
    is_earned boolean default false,
    unique (user_id, achievement_id)
);
