create table locations
(
    id int generated always as identity primary key,
    code text not null unique check (code != ''),
    place text not null unique check (place != '')
);
