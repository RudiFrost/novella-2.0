create table items_to_encounters
(
    encounter_id int references encounters(id) not null,
    item_id int references items(id) not null,
    unique(encounter_id, item_id)
);
