create table items_to_users
(
    user_id int references users(id) not null,
    item_id int references items(id) not null,
    unique (user_id, item_id)
);
