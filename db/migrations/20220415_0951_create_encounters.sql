create table encounters
(
    id int generated always as identity primary key,
    location_id int references locations,
    user_id int references users,
    encounter_type_id int references encounter_types,
    enemy_id int references enemies,
    friend_id int references friends
);
