create table achievements
(
    id int generated always as identity primary key,
    code text unique not null check (code != ''),
    title text unique not null check (title != '')
);
