create table friends
(
    id int generated always as identity primary key,
    name text not null unique check (name != ''),
    heal_amount int not null check (hp <= 50)
);
