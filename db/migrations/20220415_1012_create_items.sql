create table items
(
    id int generated always as identity primary key,
    title text not null unique check (title != ''),
    price int not null check (price <= 500),
    attack_power int not null check (attack_power <= 100)
);
