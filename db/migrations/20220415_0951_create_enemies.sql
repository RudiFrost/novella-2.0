create table enemies
(
    id int generated always as identity primary key,
    name text not null unique check (name != ''),
    hp int not null check (hp <= 300),
    attack_power int not null check (attack_power <= 50)
);
