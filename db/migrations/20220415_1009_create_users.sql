create table users
(
    id int generated always as identity primary key,
    name text not null unique check (name != ''),
    money int,
    attack int,
    hp int not null check (hp <= 100),
);
