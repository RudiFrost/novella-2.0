create table encounter_types
(
    id int generated always as identity primary key,
    code text not null unique check (code != ''),
    name text not null unique check (name != '')
);
