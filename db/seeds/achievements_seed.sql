insert into achievements (code, title)
values  ('rnd_master', 'Успешно завершить игру'),
        ('greedy', 'Сохранить 50+ монет'),
        ('first_blood', 'И пусть все начнется по новой'),
        ('phantom', 'Увернуться от 3 атак подряд');
