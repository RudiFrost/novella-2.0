insert into locations (code, place)
values  ('swamp', 'Болотистая местность'),
        ('mountain', 'Горная возвышенность'),
        ('desert', 'Пустыня'),
        ('trail', 'Мрачная тропа'),
        ('jungles', 'Джунгли'),
        ('lake', 'Гнилое озеро');
