package newNovella.script;

import newNovella.BaseModel;
import org.junit.jupiter.api.DisplayName;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

@DisplayName("TestScript")
public class TestScript {

    public String select = "SELECT %s FROM %s where %s = '%s'";
    protected void executeClean() {
        try {
            Statement statement = BaseModel.getConnection().createStatement();
            String[] executeDelete = {"DELETE from achievements;",
                    "DELETE from achievements_to_users;",
                    "DELETE from encounters;",
                    "DELETE from encounter_types;",
                    "DELETE from enemies;",
                    "DELETE from friends;",
                    "DELETE from locations;",
                    "DELETE from users;",
                    "DELETE from items;"};
            for (String query : executeDelete) {
                statement.execute(query);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    protected void execute(String query) {
        try {
            Statement statement = BaseModel.getConnection().createStatement();
            statement.execute(query);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    protected boolean executeCheck(String query) {
        boolean answer = false;
        try {
            Statement statement = BaseModel.getConnection().createStatement();
            ResultSet resultSet = statement.executeQuery(query);
            while (resultSet.next()){
                answer = resultSet.getBoolean("bool");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        System.out.println(answer);
        if (!answer){
            return false;
        } else {
            return answer;
        }
    }

    protected static <T> ArrayList<T> executeSelect(String query, BaseModel.classModel<T> model){
        ArrayList<T> newObject = new ArrayList<T>();
        try {
            Statement statement = BaseModel.getConnection().createStatement();
            ResultSet resultSet = statement.executeQuery(query);

            while (resultSet.next()) {
                newObject.add(model.newModel(resultSet));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return newObject;
    }
}
