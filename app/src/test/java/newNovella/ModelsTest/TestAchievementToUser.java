package newNovella.ModelsTest;

import newNovella.BaseModel;
import newNovella.models.Achievement;
import newNovella.models.AchievementToUser;
import newNovella.models.User;
import newNovella.script.TestScript;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.sql.SQLException;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("TestAchievement")
public class TestAchievementToUser extends TestScript {

    private static final String TABLE_NAME = "achievements_to_users";
    private static final String[] COLUMNS = {"id", "user_id", "achievement_id", "is_earned"};
    private String query = String.format(select, String.join(", ", COLUMNS), TABLE_NAME, COLUMNS[1], "rnd_master");
    @BeforeEach
    public void beforeEach() {
        executeClean();
        BaseModel.executeUpdate("insert into achievements (code, title)\n" +
                "values  ('rnd_master', 'Успешно завершить игру'),\n" +
                "        ('greedy', 'Сохранить 50+ монет'),\n" +
                "        ('first_blood', 'И пусть все начнется по новой'),\n" +
                "        ('phantom', 'Увернуться от 3 атак подряд');");

        BaseModel.executeUpdate("insert into users (name, money, attack, hp)\n" +
                "  values  ('lll', 100, 25, 100),\n" +
                "          ('kkk', 100, 25, 100)\n");
    }

    @Test
    public void getAll() throws SQLException {
        AchievementToUser.create(User.getByName("lll").getId(), Achievement.getByCode("rnd_master").getId());
        AchievementToUser.create(User.getByName("lll").getId(), Achievement.getByCode("greedy").getId());
        assertEquals(AchievementToUser.getAll().size(), 2);
    }

    @Test
    public void trueEarned() throws SQLException {
        AchievementToUser.create(User.getByName("lll").getId(), Achievement.getByCode("rnd_master").getId());
        AchievementToUser ach = AchievementToUser.getById(1);
        ach.setEarned(true);
        assertTrue(ach.isEarned());
    }

}

