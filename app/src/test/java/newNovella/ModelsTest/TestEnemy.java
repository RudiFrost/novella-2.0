package newNovella.ModelsTest;

import newNovella.script.TestScript;
import newNovella.models.Enemy;
import newNovella.BaseModel;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.sql.SQLException;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

@DisplayName("TestEnemy")
public class TestEnemy extends TestScript {

    private static final String[] COLUMNS = {"id", "name", "hp", "attack_power"};

    private static final String TABLE_NAME = "enemies";
    private String query = String.format(select, String.join(", ", COLUMNS), TABLE_NAME, COLUMNS[1], "Gaper");


    @BeforeEach
    public void beforeEach() {
        executeClean();
        BaseModel.executeUpdate("insert into enemies (name, hp, attack_power)\n" +
                "  values  ('Gaper', 100, 10),\n" +
                "          ('GlassFly', 10, 20),\n" +
                "          ('Monstrino', 300, 10);\n" +
                "\n");
    }

    @Test
    public void getAll() throws SQLException {
        ArrayList<Enemy> enemie = Enemy.getAll();
        assertEquals(enemie.size(), 3);
    }

    @Test
    public void getByCode() throws SQLException {
        Enemy enemie = executeSelect(query, Enemy.model).get(0);
        Enemy getEnemy = Enemy.getByName(enemie.getName());
        assertEquals(getEnemy.getName(), enemie.getName());
    }

    @Test
    public void create() throws SQLException {
        Enemy.create("loh", 10, 10);

        Enemy en = executeSelect(String.format(select, String.join(", ", COLUMNS), TABLE_NAME, COLUMNS[1], "loh"), Enemy.model).get(0);

        assertEquals("loh", en.getName());
    }

    @Test
    public void getEnemyRandom() throws SQLException {
        Enemy en = Enemy.getEnemyRandom();
        assertNotNull(en.getName());
    }


    @Test
    public void update() throws SQLException {
        Enemy.update("Gaper", 20, 20);
        Enemy en = executeSelect(query, Enemy.model).get(0);
        assertEquals(20, en.getHp());
    }

    @Test
    public void delete() throws SQLException {
        Enemy.delete("Gaper");
       assertEquals(Enemy.getAll().size(), 2);
    }

}

