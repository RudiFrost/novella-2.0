package newNovella.ModelsTest;

import newNovella.BaseModel;
import newNovella.models.Location;
import newNovella.script.TestScript;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.sql.SQLException;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("TestLocation")
public class TestLocation extends TestScript {

    private static final String TABLE_NAME = "locations";
    private static final String[] COLUMNS = {"id", "code", "place"};
    private String query = String.format(select, String.join(", ", COLUMNS), TABLE_NAME, COLUMNS[1], "swamp");
    @BeforeEach
    public void beforeEach() {
        executeClean();
        BaseModel.executeUpdate("insert into locations (code, place)\n" +
                "  values  ('swamp', 'Болотистая местность'),\n" +
                "          ('mountain', 'Горная возвышенность'),\n" +
                "          ('desert', 'Пустыня'),\n" +
                "          ('trail', 'Мрачная тропа'),\n" +
                "          ('jungles', 'Джунгли'),\n" +
                "          ('lake', 'Гнилое озеро');\n");
    }

    @Test
    public void getAll() throws SQLException {
        ArrayList<Location> location = Location.getAll();
        assertEquals(location.size(), 6);
    }

    @Test
    public void getByCode() throws SQLException {
        Location location = executeSelect(query, Location.model).get(0);
        Location getLocation = Location.getByCode(location.getCode());
        assertEquals(getLocation.getCode(), location.getCode());
    }

    @Test
    public void create() throws SQLException {
        Location.create("moh", "Мох");
        Location en = executeSelect(String.format(select, String.join(", ", COLUMNS), TABLE_NAME, COLUMNS[1], "moh"), Location.model).get(0);

        assertEquals("moh", en.getCode());
    }

    @Test
    public void getLocationRandom() throws SQLException {
        Location lc = Location.getLocationsRandom();
        assertNotNull(lc.getCode());
    }


    @Test
    public void update() throws SQLException {
        Location.update("swamp", "Mоох");
        Location lc = executeSelect(query, Location.model).get(0);
        assertEquals("Mоох", lc.getPlace());
    }

    @Test
    public void delete() throws SQLException {
        Location.delete("swamp");
        assertEquals(Location.getAll().size(), 5);
    }

}

