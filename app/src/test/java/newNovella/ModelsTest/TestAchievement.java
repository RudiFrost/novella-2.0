package newNovella.ModelsTest;

import newNovella.BaseModel;
import newNovella.models.Achievement;
import newNovella.script.TestScript;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.sql.SQLException;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DisplayName("TestAchievement")
public class TestAchievement extends TestScript {

    private static final String TABLE_NAME = "achievements";
    private static final String[] COLUMNS = {"id", "code", "title"};
    private String query = String.format(select, String.join(", ", COLUMNS), TABLE_NAME, COLUMNS[1], "rnd_master");
    @BeforeEach
    public void beforeEach() {
        executeClean();
        BaseModel.executeUpdate("insert into achievements (code, title)\n" +
                "values  ('rnd_master', 'Успешно завершить игру'),\n" +
                "        ('greedy', 'Сохранить 50+ монет'),\n" +
                "        ('first_blood', 'И пусть все начнется по новой'),\n" +
                "        ('phantom', 'Увернуться от 3 атак подряд');");
    }

    @Test
    public void getAll() throws SQLException {
        ArrayList<Achievement> achievement = Achievement.getAll();
        assertEquals(achievement.size(), 4);
    }

    @Test
    public void getByCode() throws SQLException {
        Achievement achievement = executeSelect(query, Achievement.achievementLambda).get(0);
        Achievement getAchievement = Achievement.getByCode(achievement.getCode());
        assertEquals(getAchievement.getCode(), achievement.getCode());
    }

    @Test
    public void create() throws SQLException {
        Achievement.create("moh", "lol");
        Achievement ach = executeSelect(String.format(select, String.join(", ", COLUMNS), TABLE_NAME, COLUMNS[1], "moh"), Achievement.achievementLambda).get(0);

        assertEquals("lol", ach.getTitle());
    }

    @Test
    public void update() throws SQLException {
        Achievement.update("rnd_master", "kkk");
        Achievement ach = executeSelect(query, Achievement.achievementLambda).get(0);
        assertEquals("kkk", ach.getTitle());
    }

    @Test
    public void delete() throws SQLException {
        Achievement.delete("rnd_master");
        assertEquals(Achievement.getAll().size(), 3);
    }

}

