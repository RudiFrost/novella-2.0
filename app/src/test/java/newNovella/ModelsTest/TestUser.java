package newNovella.ModelsTest;

import newNovella.BaseModel;
import newNovella.models.Achievement;
import newNovella.models.Item;
import newNovella.models.User;
import newNovella.script.TestScript;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.sql.SQLException;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DisplayName("TestUser")
public class TestUser extends TestScript {

    private static final String TABLE_NAME = "users";
    private static final String[] COLUMNS = {"id", "name", "money", "attack", "hp"};
    private String query = String.format(select, String.join(", ", COLUMNS), TABLE_NAME, COLUMNS[1], "lll");
    @BeforeEach
    public void beforeEach() {
        executeClean();
        BaseModel.executeUpdate("insert into users (name, money, attack, hp)\n" +
                "  values  ('lll', 100, 25, 100),\n" +
                "          ('kkk', 100, 25, 100)\n");

        BaseModel.executeUpdate("insert into achievements (code, title)\n" +
                "values  ('rnd_master', 'Успешно завершить игру'),\n" +
                "        ('greedy', 'Сохранить 50+ монет'),\n" +
                "        ('first_blood', 'И пусть все начнется по новой'),\n" +
                "        ('phantom', 'Увернуться от 3 атак подряд');");

        BaseModel.executeUpdate("insert into items (title, price, attack_power)\n" +
                "    values ('Деревянный меч', 100, 5),\n" +
                "           ('Железный меч', 200, 10),\n" +
                "           ('Нунчаки', 150, 20),\n" +
                "           ('Топор', 260, 25);");
    }

    @Test
    public void getAll() throws SQLException {
        ArrayList<User> user = User.getAll();
        assertEquals(user.size(), 2);
    }

    @Test
    public void getByName() throws SQLException {
        User user = executeSelect(query, User.userLambda).get(0);
        User getUser = User.getByName(user.getName());
        assertEquals(getUser.getName(), user.getName());
    }

    @Test
    public void create() throws SQLException {
        User.create("loh");
        User us = executeSelect(String.format(select, String.join(", ", COLUMNS), TABLE_NAME, COLUMNS[1], "loh"), User.userLambda).get(0);
        assertEquals("loh", us.getName());
    }

    @Test
    public void addAchievement() throws SQLException {
        User us = User.getByName("lll");
        us.addAchievement(Achievement.getByCode("rnd_master"));
        us.addAchievement(Achievement.getByCode("greedy"));
        assertEquals(us.getAchievements().get(0).getCode(), "rnd_master");
        assertEquals(us.getAchievements().get(1).getCode(), "greedy");
    }

    @Test
    public void addItem() throws SQLException {
        User us = User.getByName("lll");
        us.addItem(Item.getByTitle("Деревянный меч"));
        us.addItem(Item.getByTitle("Нунчаки"));
        assertEquals(us.getItems().get(0).getTitle(), "Деревянный меч");
        assertEquals(us.getItems().get(1).getTitle(), "Нунчаки");
    }

    @Test
    public void update() throws SQLException {
        User.update("lll", 5,25, 5);
        User us = executeSelect(query, User.userLambda).get(0);
        assertEquals(5, us.getMoney());
    }

    @Test
    public void delete() throws SQLException {
        User.delete("lll");
        assertEquals(User.getAll().size(), 1);
    }

}

