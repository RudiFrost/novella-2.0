package newNovella.ModelsTest;

import newNovella.BaseModel;
import newNovella.models.EncounterType;
import newNovella.script.TestScript;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.sql.SQLException;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@DisplayName("TestEncounterType")
public class TestEncounterType extends TestScript {

    private static final String TABLE_NAME = "encounter_types";
    private static final String[] COLUMNS = {"id", "code", "name"};
    private String query = String.format(select, String.join(", ", COLUMNS), TABLE_NAME, COLUMNS[1], "friend");
    @BeforeEach
    public void beforeEach() {
        executeClean();
        BaseModel.executeUpdate("insert into encounter_types (code, name)\n" +
                "  values  ('friend', 'Друг'),\n" +
                "          ('enemy', 'Враг'),\n" +
                "          ('shop', 'Магазин');\n");
    }

    @Test
    public void getAll() throws SQLException {
        ArrayList<EncounterType> encounterType = EncounterType.getAll();
        assertEquals(encounterType.size(), 3);
    }

    @Test
    public void getByCode() throws SQLException {
        EncounterType encounterType = executeSelect(query, EncounterType.model).get(0);
        EncounterType getEncounterType = EncounterType.getByCode(encounterType.getCode());
        assertEquals(getEncounterType.getCode(), encounterType.getCode());
    }

    @Test
    public void create() throws SQLException {
        EncounterType.create("moh", "Мох");
        EncounterType en = executeSelect(String.format(select,
                String.join(", ", COLUMNS), TABLE_NAME, COLUMNS[1], "moh"), EncounterType.model).get(0);

        assertEquals("moh", en.getCode());
    }

    @Test
    public void getEncounterTypeRandom() throws SQLException {
        EncounterType lc = EncounterType.getEncounterTypesRandom();
        assertNotNull(lc.getCode());
    }


    @Test
    public void update() throws SQLException {
        EncounterType.update("friend", "Дружище");
        EncounterType lc = executeSelect(query, EncounterType.model).get(0);
        assertEquals("Дружище", lc.getName());
    }

    @Test
    public void delete() throws SQLException {
        EncounterType.delete("friend");
        assertEquals(EncounterType.getAll().size(), 2);
    }

}

