package newNovella.ModelsTest;

import newNovella.BaseModel;
import newNovella.models.Item;
import newNovella.models.User;
import newNovella.script.TestScript;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.sql.SQLException;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@DisplayName("TestItem")
public class TestItem extends TestScript {

    private static final String TABLE_NAME = "items";
    private static final String[] COLUMNS = {"id", "title", "price", "attack_power"};
    private String query = String.format(select, String.join(", ", COLUMNS), TABLE_NAME, COLUMNS[1], "Деревянный меч");
    @BeforeEach
    public void beforeEach() {
        executeClean();
        BaseModel.executeUpdate("insert into items (title, price, attack_power)\n" +
                "    values ('Деревянный меч', 100, 5),\n" +
                "           ('Железный меч', 200, 10),\n" +
                "           ('Нунчаки', 150, 20),\n" +
                "           ('Топор', 260, 25);");

        BaseModel.executeUpdate("insert into users (name, money, attack, hp)\n" +
                "  values  ('lll', 100, 25, 100),\n" +
                "          ('kkk', 100, 25, 100)\n");

    }

    @Test
    public void getAll() throws SQLException {
        ArrayList<Item> item = Item.getAll();
        assertEquals(item.size(), 4);
    }

    @Test
    public void getByTitle() throws SQLException {
        Item item = executeSelect(query, Item.model).get(0);
        Item getItem = Item.getByTitle(item.getTitle());
        assertEquals(getItem.getTitle(), item.getTitle());
    }

    @Test
    public void addUsers() throws SQLException {
        Item it = Item.getByTitle("Деревянный меч");
        it.addUser(User.getByName("lll"));
        it.addUser(User.getByName("kkk"));
        assertEquals(it.getUsers().get(0).getName(), "lll");
        assertEquals(it.getUsers().get(1).getName(), "kkk");
    }

    @Test
    public void create() throws SQLException {
        Item.create("kkk", 10, 10);
        Item fr = executeSelect(String.format(select, String.join(", ", COLUMNS), TABLE_NAME, COLUMNS[1], "kkk"), Item.model).get(0);

        assertEquals(10, fr.getPrice());
    }

    @Test
    public void getItemRandom() throws SQLException {
        ArrayList<Item> it = Item.getShopListRandom();
        assertEquals(it.size(), 3);
    }


    @Test
    public void update() throws SQLException {
        Item.update("lol", 10, 20);
        Item fr = executeSelect(query, Item.model).get(0);
        assertEquals(10, fr.getPrice());
    }

    @Test
    public void delete() throws SQLException {
        Item.delete("Деревянный меч");
        assertEquals(Item.getAll().size(), 3);
    }

}

