package newNovella.ModelsTest;

import newNovella.BaseModel;
import newNovella.models.Friend;
import newNovella.script.TestScript;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.sql.SQLException;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@DisplayName("TestFriend")
public class TestFriend extends TestScript {

    private static final String TABLE_NAME = "friends";
    private static final String[] COLUMNS = {"id", "name", "heal_amount"};
    private String query = String.format(select, String.join(", ", COLUMNS), TABLE_NAME, COLUMNS[1], "Bumbo");
    @BeforeEach
    public void beforeEach() {
        executeClean();
        BaseModel.executeUpdate("insert into friends (name, heal_amount)\n" +
                "  values  ('Bumbo', 30),\n" +
                "          ('Bumbino', 50),\n" +
                "          ('Bumbo?', -20);");
    }

    @Test
    public void getAll() throws SQLException {
        ArrayList<Friend> friend = Friend.getAll();
        assertEquals(friend.size(), 3);
    }

    @Test
    public void getByCode() throws SQLException {
        Friend friend = executeSelect(query, Friend.model).get(0);
        Friend getFriend = Friend.getByName(friend.getName());
        assertEquals(getFriend.getName(), friend.getName());
    }

    @Test
    public void create() throws SQLException {
        Friend.create("moh", 10);
        Friend fr = executeSelect(String.format(select, String.join(", ", COLUMNS), TABLE_NAME, COLUMNS[1], "moh"), Friend.model).get(0);

        assertEquals(10, fr.getHealAmount());
    }

    @Test
    public void getFriendRandom() throws SQLException {
        Friend fr = Friend.getFriendRandom();
        assertNotNull(fr.getName());
    }


    @Test
    public void update() throws SQLException {
        Friend.update("Bumbo", 10);
        Friend fr = executeSelect(query, Friend.model).get(0);
        assertEquals(10, fr.getHealAmount());
    }

    @Test
    public void delete() throws SQLException {
        Friend.delete("Bumbo");
        assertEquals(Friend.getAll().size(), 2);
    }

}

