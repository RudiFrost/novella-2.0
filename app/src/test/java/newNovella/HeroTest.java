package newNovella;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;

class HeroTest {
    private Hero hero;

    @BeforeEach
    public void initTest() {
        hero = new Hero("Denis");

    }

    @Test void checkGetName() {
        assertEquals("Denis", hero.getName(), "good");
    }

    @Test void checkGetHp() {
        hero.setHp(100);
        assertEquals(100, hero.getHp(), "good");
    }

}
