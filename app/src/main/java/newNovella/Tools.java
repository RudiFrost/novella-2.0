package newNovella;

import javax.servlet.http.HttpServletRequest;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Tools {
    private static final String REGEX = "/node_action_dispatcher/(\\d)";
    private static final String REGEXSHOP = "/by_in_shop/(\\d)";

    public static boolean isNumeric(String string) {
        String regex = "[0-9]+[\\.]?[0-9]*";
        return Pattern.matches(regex, string);
    }

    public static String getActionFromRequest(HttpServletRequest request) {
        Matcher matcher = Pattern.compile(REGEX).matcher(request.getRequestURL().toString());
        return (matcher.find()) ? matcher.group(1) : "not found";
    }

    public static String getActionFromREquestShop(HttpServletRequest request) {
        Matcher matcher = Pattern.compile(REGEXSHOP).matcher(request.getRequestURL().toString());
        return (matcher.find()) ? matcher.group(1) : "not found";
    }

    public static void updateTableEnemy() {
        executeClean();
        BaseModel.executeUpdate("insert into enemies (name, hp, attack_power)\n" +
                "  values  ('Gaper', 100, 10),\n" +
                "          ('GlassFly', 10, 20),\n" +
                "          ('Monstrino', 300, 10);\n" +
                "\n");
    }

    protected static void executeClean() {
        try {
            Statement statement = BaseModel.getConnection().createStatement();
            String[] executeDelete = {"DELETE from enemies;"};
            for (String query : executeDelete) {
                statement.execute(query);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}