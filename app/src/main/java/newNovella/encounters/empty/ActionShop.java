package newNovella.encounters.empty;

import newNovella.encounters.Action;

import java.sql.SQLException;
import newNovella.models.User;

import static newNovella.encounters.empty.EmptyProcess.itemList;

public class ActionShop extends Action {


    public ActionShop(String action) {
        super(action);
        endless = true;
    }

    public static String byItem(int userInput) throws SQLException {
        User us = User.getByName(newNovella.State.name);

        boolean canBuy = us.getMoney() >= itemList.get(userInput - 1).getPrice();
        if (canBuy) {
            us.setMoney(us.getMoney() - itemList.get(userInput - 1).getPrice());
            User.updateMoney(newNovella.State.name, us.getMoney());
            us.setAttack(us.getAttack() +  itemList.get(userInput - 1).getAttack_power());
            User.updateAttack(newNovella.State.name, us.getAttack());

        }

        return canBuy ? "Вы совершили покупку": "Недостаточно денег";
    }

    public void act() throws Exception {

    }
}


