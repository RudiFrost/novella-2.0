package newNovella.encounters.empty;
import newNovella.encounters.Action;

public class ActionStay extends Action {


    ActionStay(String action) {
        super(action);
        endless = true;
    }

    public void act() throws Exception {
    }
}
