package newNovella.encounters.empty;

import newNovella.models.Item;

import java.sql.SQLException;
import java.util.ArrayList;
import newNovella.encounters.Encount;


public class EmptyProcess extends Encount {
    public static ArrayList<Item> itemList;

    static {
        try {
            itemList = Item.getShopListRandom();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }


    public EmptyProcess(String message) {
        super(message);
        actions.add(new ActionStay("0) постоять"));
        actions.add(new ActionShop("1) зайти в магазин"));
        actions.add(new ActionPassage("2) перейти на следующий уровень"));
    }
}