package newNovella.encounters;

abstract public class Action {

    public boolean endless;
    protected String action;

    public Action() {}
    public Action(String action) {
        this.action = action;
    }

    public boolean isEndless() {
        return endless;
    }

    public String toString() {
        return action;
    }

    abstract public void act() throws Exception;

}
