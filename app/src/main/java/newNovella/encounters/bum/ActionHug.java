package newNovella.encounters.bum;

import newNovella.State;
import newNovella.encounters.Action;
import newNovella.models.Friend;
import newNovella.models.User;


import static newNovella.encounters.bum.BumProcess.friendStats;

public class ActionHug extends Action {


    ActionHug(String action) {
        super(action);
    }

    public void act() throws Exception {
        friendStats = new Friend().getFriendRandom();

        User user = User.getByName(State.name);

        if (friendStats.getHeal_amount() < 0) {
            User.updateHp(newNovella.State.name, (int)(user.getHp() + friendStats.getHeal_amount()));
        } else {
            if (User.getByName(State.name).getHp() <= 50) {
                User.updateHp(newNovella.State.name, (int)(user.getHp() + friendStats.getHeal_amount()));
            }
        }
    }

}
