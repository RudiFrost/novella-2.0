package newNovella.encounters.bum;

import newNovella.encounters.Encount;
import newNovella.models.Friend;

import java.sql.SQLException;

public class BumProcess extends Encount {

    public static Friend friendStats;

    static {
        try {
            friendStats = Friend.getFriendRandom();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public BumProcess(String message) throws SQLException {
        super(message);
        actions.add(new ActionHello("0) поздороваться"));
        actions.add(new ActionHug("1) обнять(пополните хп если у вас < 50%)"));
    }
}