package newNovella.encounters;

import newNovella.encounters.bum.BumProcess;
import newNovella.encounters.empty.EmptyProcess;
import newNovella.encounters.enemy.EnemyProcess;
import newNovella.models.EncounterType;

import java.util.*;

public class Encount {
    public String message;
    public ArrayList<Action> actions = new ArrayList<Action>();
    public Action action;

    public Encount(String message) {
        this.message = message;
    }

    public String getMessage() {
        return this.message;
    }

    public static Encount generate() throws Exception {
        EncounterType encounterType = EncounterType.getEncounterTypesRandom();

        if (encounterType.getCode().equals("friend")) {
            return new BumProcess("friend");
        } else if (encounterType.getCode().equals("shop")) {
            return new EmptyProcess("shop");
        }
        return new EnemyProcess("enemy");
    }
}