package newNovella.encounters.enemy;

import newNovella.encounters.Action;
import newNovella.State;
import newNovella.models.User;
import newNovella.models.Enemy;
import newNovella.Tools;

public class ActionLastHit extends Action {


    ActionLastHit(String action) {
        super(action);
        endless = true;
    }

    public void act() throws Exception {
        EnemyProcess.enemyStats = Enemy.getEnemyRandom();
        Enemy enemy = EnemyProcess.enemyStats;
        User user = User.getByName(State.name);
        State.flagRandomLuck = false;

        if (enemy.getHp() > 0 && user.getHp() > 0) {
            enemy.setHp(enemy.getHp() - user.getAttack() * 2);
            Enemy.updateHp(enemy.getName(), enemy.getHp());
            if ((int) (Math.random() * 9) != 1) {
                User.updateHp(user.getName(), (int)(user.getHp() - enemy.getAttack_power() * 3));
            } else {
                State.flagRandomLuck = true;
            }
        }

        if (user.getHp() <= 0) {
            User.delete(user.getName());
        } else if (enemy.getHp() <= 0) {
            Tools.updateTableEnemy();
            State.randomMoney = (int) ((Math.random() * 50) + 100);
            User.updateMoney(newNovella.State.name, State.randomMoney + user.getMoney());
        }

    }
}
