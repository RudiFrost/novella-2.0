package newNovella.encounters.enemy;

import newNovella.models.Enemy;
import newNovella.encounters.Encount;

import java.sql.SQLException;

public class EnemyProcess extends Encount {

    public static Enemy enemyStats;
    static {
        try {
            enemyStats = Enemy.getEnemyRandom();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
    public EnemyProcess(String message) throws SQLException {
        super(message);
        actions.add(new newNovella.encounters.enemy.ActionAttack("0) Нанести удар по врагу(урон равен 25)"));
        actions.add(new newNovella.encounters.enemy.ActionLastHit("1) Попытаться добить врага(нанести двойной урон и в случае неудачи получить тройной)"));
    }

}