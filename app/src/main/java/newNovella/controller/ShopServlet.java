package newNovella.controller;

import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.*;
import java.sql.SQLException;
import javax.servlet.annotation.WebServlet;


@WebServlet("/by_in_shop/*")
public class ShopServlet extends HttpServlet {

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        response.setContentType("text/html; charset=UTF-8");
        request.setCharacterEncoding("UTF-8");

        int i = Integer.parseInt(newNovella.Tools.getActionFromREquestShop(request));
        try {
            String statusByItem = newNovella.encounters.empty.ActionShop.byItem(i);
            request.setAttribute("statusByItem", statusByItem);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        request.getRequestDispatcher("/newNovella/view/shop_status.jsp").forward(request, response);
    }



}