package newNovella.controller;

import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.*;
import javax.servlet.annotation.WebServlet;
import newNovella.models.Location;
import newNovella.Node;
import newNovella.encounters.Encount;
@WebServlet("/menu")
public class MenuServlet extends HttpServlet {

    public static String name;

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        renderJsp(request, response);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        try {
            Node node = new Node();
            node.setEncount(Encount.generate());
            newNovella.State.node = node;
            newNovella.State.location = Location.getLocationsRandom();
        } catch (Exception e) {
            e.printStackTrace();
        }

        renderJsp(request, response);
    }

    private void renderJsp(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html; charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        request.setAttribute("node", newNovella.State.node);

        request.getRequestDispatcher("/newNovella/view/menu.jsp").forward(request, response);
    }


}
