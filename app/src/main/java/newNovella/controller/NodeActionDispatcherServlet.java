package newNovella.controller;

import newNovella.State;
import newNovella.encounters.enemy.EnemyProcess;
import newNovella.models.User;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.*;
import javax.servlet.annotation.WebServlet;
import newNovella.encounters.Action;

@WebServlet("/node_action_dispatcher/*")
public class NodeActionDispatcherServlet extends HttpServlet {

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        response.setContentType("text/html; charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        int i = Integer.parseInt(newNovella.Tools.getActionFromRequest(request));
        Action action = State.node.getEncount().actions.get(i);
        try {
            State.hpUserBefore = User.getByName(State.name).getHp();
            State.hpEnemyBefore = EnemyProcess.enemyStats.getHp();
            action.act();
            State.hpUserAfter = User.getByName(State.name).getHp();
            State.hpEnemyAfter = EnemyProcess.enemyStats.getHp();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }


        request.getRequestDispatcher("/newNovella/view/" + action.getClass().getSimpleName() + ".jsp").forward(request, response);
    }

}
