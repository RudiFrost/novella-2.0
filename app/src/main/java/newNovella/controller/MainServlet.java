package newNovella.controller;

import newNovella.models.User;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.*;
import java.sql.SQLException;
import javax.servlet.annotation.WebServlet;

@WebServlet("/main")
public class MainServlet extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        response.setContentType("text/html; charset=UTF-8");
        request.setCharacterEncoding("UTF-8");

        request.getRequestDispatcher("/newNovella/view/main_site.jsp").forward(request, response);
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        response.setContentType("text/html; charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        String name = request.getParameter("userName");

        if (name.length() == 0 || name.trim().isEmpty()) {
            request.setAttribute("message", "Вы не ввели имя, введите его пж");
            request.getRequestDispatcher("/newNovella/view/go_to_main.jsp").forward(request, response);
        } else {
            try {
                if (User.getByName(name) == null) {
                    User.create(name);
                    newNovella.Tools.updateTableEnemy();
                }
                newNovella.State.name = User.getByName(name).getName();
                request.setAttribute("name", User.getByName(name).getName());
                request.getRequestDispatcher("/newNovella/view/main_site_post.jsp").forward(request, response);
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }

    }
}
