package newNovella.models;

import newNovella.BaseModel;


import java.sql.SQLException;
import java.util.ArrayList;

/**
 * The Enemy class is used to establish a connection with the database
 * and data storage
 *
 * @author Elizaveta_krasova, RudiFrost, Denchik89
 */
public class Enemy extends BaseModel {
    /**
     * @value the field stores the id of the Enemy
     */
    private int id;
    /**
     * @value the field stores the name of the Enemy
     */
    private String name;
    /**
     * @value the field stores the hp of the Enemy
     */
    private int hp;
    /**
     * @value the field stores the attack of the Enemy
     */
    private int attackPower;
    /**
     * @value the field stores the titles of the table columns
     */
    private static final String[] COLUMNS = {"id", "name", "hp", "attack_power"};
    /**
     * @value the field stores the title of the table
     */
    private static final String TABLE_NAME = "enemies";

    /**
     * method getId
     *
     * @return enemy id
     */
    public int getId() {
        return id;
    }

    /**
     * method getName
     *
     * @return enemy's name
     */
    public String getName() {
        return name;
    }

    /**
     * method setName
     *
     * @param name enemy's name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * method getHp
     *
     * @return enemy hp
     */
    public int getHp() {
        return hp;
    }

    /**
     * method setHp
     *
     * @param hp enemy hp
     */
    public void setHp(int hp) {
        this.hp = hp;
    }

    /**
     * method getAttackPower
     *
     * @return enemy's attackPower
     */
    public int getAttack_power() {
        return attackPower;
    }

    /**
     * method setAttack_power
     *
     * @param attackPower enemy's attackPower
     */
    public void setAttack_power(int attackPower) {
        this.attackPower = attackPower;
    }

    /**
     * Enemy constructor
     *
     * @param id          id enemy
     * @param name        name enemy
     * @param hp          hp enemy
     * @param attackPower attackPower enemy
     */
    public Enemy(int id, String name, int hp, int attackPower) {
        this.id = id;
        this.name = name;
        this.hp = hp;
        this.attackPower = attackPower;
    }

    public Enemy() {

    }

    /**
     * @value an instance of the BaseModel class
     */
    private static BaseModel<Enemy> bdConnection = new BaseModel<Enemy>();
    /**
     * lambda function that creates an instance of the Enemy class and returns it
     */
    public static BaseModel.classModel<Enemy> model = (resultSet) -> {
        Enemy enemie = null;
        try {
            enemie = new Enemy(
                    resultSet.getInt(COLUMNS[0]),
                    resultSet.getString(COLUMNS[1]),
                    resultSet.getInt(COLUMNS[2]),
                    resultSet.getInt(COLUMNS[3])
            );
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return enemie;
    };

    /**
     * executeWithResultSet method
     * execution with returning of the received data of the select
     *
     * @param query request
     * @return selection data array
     */
    public static ArrayList<Enemy> executeWithResultSet(String query) throws SQLException {
        return bdConnection.executeSelect(query, model);
    }

    /**
     * executeWithResultSet method
     * execution without returning of the received data of the select
     *
     * @param query request
     * @return selection data not null
     */
    public static boolean executeWithoutResultSet(String query, int id) throws SQLException {
        bdConnection.executeUpdate(query);
        return getById(id) != null;
    }

    /**
     * executeWithoutResultSetByName method
     * execution without returning of the received data of the select
     *
     * @param query request
     * @param name  select name
     * @return selection data not null
     */
    public static boolean executeWithoutResultSetByName(String query, String name) throws SQLException {
        bdConnection.executeUpdate(query);
        return getByName(name) != null;
    }

    /**
     * static method getAll
     *
     * @return an array of all values in the table
     */
    public static ArrayList<Enemy> getAll() throws SQLException {
        String query = String.format("SELECT %s FROM %s", String.join(", ", COLUMNS), TABLE_NAME);
        return executeWithResultSet(query);
    }

    /**
     * static method getById
     *
     * @return Enemy with given name
     */
    public static Enemy getByName(String name) throws SQLException {
        String query = String.format("SELECT %s FROM %s WHERE %s = '%s'",
                String.join(", ", COLUMNS), TABLE_NAME, COLUMNS[1], name);
        ArrayList<Enemy> enemies = executeWithResultSet(query);
        return enemies.get(0);
    }

    /**
     * static method getById
     *
     * @return Enemy with given id
     */
    public static Enemy getById(int id) throws SQLException {
        String query = String.format("SELECT %s FROM %s WHERE %s = '%s'",
                String.join(", ", COLUMNS), TABLE_NAME, COLUMNS[0], id);
        ArrayList<Enemy> enemies = executeWithResultSet(query);
        return enemies.get(0);
    }

    /**
     * static method getEnemyRandom
     *
     * @return random Enemy
     */
    public static Enemy getEnemyRandom() throws SQLException {
        String query = String.format("SELECT %s FROM %s ORDER BY RANDOM() LIMIT 1",
                String.join(", ", COLUMNS), TABLE_NAME);
        ArrayList<Enemy> enemies = executeWithResultSet(query);
        return enemies.get(0);
    }

    /**
     * static method create      *  new enemy
     *
     * @param name        name which we add to the database
     * @param hp          hp which we add to the database
     * @param attackPower attackPower which we add to the database
     */
    public static boolean create(String name, int hp, int attackPower) throws SQLException {
        String query = String.format("insert into %s (%s, %s, %s) values('%s', '%s', '%s')",
                TABLE_NAME, COLUMNS[1], COLUMNS[2], COLUMNS[3], name, hp, attackPower);
        return executeWithoutResultSetByName(query, name);
    }

    /**
     * static method updateHp     *  any record in the enemy's database
     *
     * @param name        name which we update in the database
     * @param hp          hp which we update in the database
     * @param attackPower attackPower which we update in the database
     */
    public static boolean updateHp(String name, int hp) throws SQLException {
        String query = String.format("update %s set %s = '%d' where %s = '%s'",
                TABLE_NAME, COLUMNS[2], hp, COLUMNS[1], name);
        return executeWithoutResultSetByName(query, name);
    }

    /**
     * static method updateHp     *  any record in the enemy's database
     * @param name        name which we update in the database
     * @param hp          hp which we update in the database
     */
    public static boolean updateHp(String name, int hp) throws SQLException {
        String query = String.format("update %s set %s = '%d' where %s = '%s'",
                TABLE_NAME, COLUMNS[2], hp, COLUMNS[1], name);
        return executeWithoutResultSetByName(query, name);
    }

    /**
     * static method delete the existing achievement
     *
     * @param name enemy's name which we're deleting
     */
    public static void delete(String name) throws SQLException {
        String query = String.format("delete from %s where %s = '%s'", TABLE_NAME, COLUMNS[1], name);
        bdConnection.executeUpdate(query);
    }

}
