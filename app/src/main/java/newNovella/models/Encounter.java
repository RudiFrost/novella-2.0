package newNovella.models;

import newNovella.BaseModel;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 * The Encounter class is used to establish a connection with the database
 * and data storage
 *
 * @author Elizaveta_krasova, RudiFrost, Denchik89
 */
public class Encounter {
    /**
     * @value the field stores the id of the encounter
     */
    private int id;
    /**
     * @value field stores encounter's locations
     */
    private ArrayList<Location> locations = new ArrayList<Location>();
    /**
     * @value field stores encounter's users
     */
    private ArrayList<User> users = new ArrayList<User>();
    /**
     * @value field stores encounter's encounterTypes
     */
    private ArrayList<EncounterType> encounterTypes = new ArrayList<EncounterType>();
    /**
     * @value field stores encounter's enemies
     */
    private ArrayList<Enemy> enemies = new ArrayList<Enemy>();
    /**
     * @value field stores encounter's friends
     */
    private ArrayList<Friend> friends = new ArrayList<Friend>();

    /**
     * Encounter constructor
     *
     * @param id    id encounter
     * @param byId  Location encounter
     * @param byId1 User encounter
     * @param byId2 EncounterType encounter
     * @param byId3 Enemy encounter
     * @param byId4 Friend encounter
     */
    public Encounter(int id, Location byId, User byId1, EncounterType byId2, Enemy byId3, Friend byId4) {
    }

    /**
     * method getId
     *
     * @return encounter id
     */
    public int getId() {
        return id;
    }

    /**
     * method getLocations
     *
     * @return encounter's locations
     */
    public ArrayList<Location> getLocations() {
        return locations;
    }

    /**
     * method addLocation
     * adds encounter's location
     *
     * @param location adds location
     */
    public void addLocation(Location location) {
        locations.add(location);
    }

    /**
     * method getLocations
     *
     * @return encounter's locations
     */
    public ArrayList<User> getUsers() {
        return users;
    }

    /**
     * method addUser
     * adds encounter's user
     *
     * @param user adds user
     */
    public void addUser(User user) {
        users.add(user);
    }

    /**
     * method getLocations
     *
     * @return encounter's locations
     */
    public ArrayList<EncounterType> getEncounterTypes() {
        return encounterTypes;
    }

    /**
     * method addEncounterType
     * adds encounter's encounterType
     *
     * @param encounterType adds encounterType
     */
    public void addEncounterType(EncounterType encounterType) {
        encounterTypes.add(encounterType);
    }

    /**
     * method getLocations
     *
     * @return encounter's locations
     */
    public ArrayList<Enemy> getEnemies() {
        return enemies;
    }

    /**
     * method addEnemy
     * adds encounter's enemie
     *
     * @param enemie adds enemie
     */
    public void addEnemy(Enemy enemie) {
        enemies.add(enemie);
    }

    /**
     * method getLocations
     *
     * @return encounter's locations
     */
    public ArrayList<Friend> getFriends() {
        return friends;
    }

    /**
     * method addFriend
     * adds encounter's friend
     *
     * @param friend adds friend
     */
    public void addFriend(Friend friend) {
        friends.add(friend);
    }

    /**
     * @value the field stores the titles of the table columns
     */
    private static final String[] COLUMNS = {"id", "location_id", "user_id", "encounter_type_id", "enemy_id", "friend_id"};
    /**
     * @value the field stores the title of the table
     */
    private static final String TABLE_NAME = "encounters";

    /**
     * @value an instance of the BaseModel class
     */
    private static BaseModel<Encounter> bdConnection = new BaseModel<Encounter>();
    /**
     * lambda function that creates an instance of the Encounter class and returns it
     */
    private static BaseModel.classModel<Encounter> model = (resultSet) -> {
        Encounter encounter = null;
        try {
            int id = resultSet.getInt("id");
            int locationId = resultSet.getInt("location_id");
            int userId = resultSet.getInt("user_id");
            int encounterTypeId = resultSet.getInt("encounter_type_id");
            int enemyId = resultSet.getInt("enemy_id");
            int friendId = resultSet.getInt("friend_id");

            encounter = new Encounter(
                    id,
                    Location.getById(locationId),
                    User.getById(userId),
                    EncounterType.getById(encounterTypeId),
                    Enemy.getById(enemyId),
                    Friend.getById(friendId)
            );

            encounter.addLocation(Location.getById(locationId));
            encounter.addUser(User.getById(userId));
            encounter.addEncounterType(EncounterType.getById(userId));
            encounter.addEnemy(Enemy.getById(enemyId));
            encounter.addFriend(Friend.getById(friendId));

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return encounter;
    };

    /**
     * executeWithResultSet method
     * execution with returning of the received data of the select
     *
     * @param query request
     * @return selection data array
     */
    public static ArrayList<Encounter> executeWithResultSet(String query) throws SQLException {
        return bdConnection.executeSelect(query, model);
    }

    /**
     * executeWithResultSet method
     * execution without returning of the received data of the select
     *
     * @param query request
     * @return selection data not null
     */
    public static boolean executeWithoutResultSet(String query, int id) throws SQLException {
        bdConnection.executeUpdate(query);
        return getById(id) != null;
    }

    /**
     * static method getById
     *
     * @return Encounter with given id
     */
    public static Encounter getById(int id) throws SQLException {
        ArrayList<Encounter> encounters = executeWithResultSet(String.format("SELECT %s FROM %s WHERE %s in ('%s')",
                String.join(", ", COLUMNS), TABLE_NAME, COLUMNS[0], id));
        return encounters.isEmpty() ? null : encounters.get(0);
    }

    /**
     * static method getAll
     *
     * @return an array of all values in the table
     */
    public static ArrayList<Encounter> getAll() throws SQLException {
        String query = String.format("SELECT %s FROM %s", String.join(", ", COLUMNS), TABLE_NAME);
        ArrayList<Encounter> encounters = executeWithResultSet(query);
        return encounters.isEmpty() ? null : encounters;
    }

    /**
     * static method create      *  new encounter
     *
     * @param locationId      locationId which we add to the database
     * @param userId          userId which we add to the database
     * @param encounterTypeId encounterTypeId which we add to the database
     * @param enemyId         enemyId which we add to the database
     * @param friendId        friendId which we add to the database
     */
    public static void create(int locationId, int userId, int encounterTypeId, int enemyId, int friendId) throws SQLException {
        String query = String.format("insert into %s (location_id, user_id, encounterTypeId, locationId, userId) values('%s', '%s', '%s', '%s', '%s')",
                TABLE_NAME,
                locationId,
                userId,
                encounterTypeId,
                enemyId,
                friendId);
        bdConnection.executeUpdate(query);
    }

    /**
     * static method update     *  any record in the user's database
     *
     * @param id              id which we update in the database
     * @param locationId      locationId which we update in the database
     * @param userId          userId which we update in the database
     * @param encounterTypeId encounterTypeId which we update in the database
     * @param enemyId         enemyId which we update in the database
     */
    public static boolean update(int id, int locationId, int userId, int encounterTypeId, int enemyId, int friendId) throws SQLException {
        String query = String.format("update %s set location_id = '%s', user_id = '%s', encounter_type_id = '%s', enemy_id = '%s', friend_id = '%s' where id = '%s'",
                TABLE_NAME,
                locationId,
                userId,
                encounterTypeId,
                enemyId,
                friendId,
                id);
        return executeWithoutResultSet(query, id);
    }

    /**
     * static method delete the existing encounter
     *
     * @param id encounter's id which we're deleting
     */
    public static void delete(int id) throws SQLException {
        bdConnection.executeUpdate(String.format("delete from %s where id = %s", TABLE_NAME, id));
    }


}