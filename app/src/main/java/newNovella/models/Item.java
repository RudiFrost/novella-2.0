package newNovella.models;

import newNovella.BaseModel;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 * The Item class is used to establish a connection with the database
 * and data storage
 *
 * @author Elizaveta_krasova, RudiFrost, Denchik89
 */
public class Item extends BaseModel {
    /**
     * @value the field stores the id of the item
     */
    private int id;
    /**
     * @value the field stores the title of the item
     */
    private String title;
    /**
     * @value the field stores the price of the item
     */
    private int price;
    /**
     * @value the field stores the attackPower of the item
     */
    private int attackPower;
    /**
     * @value the field stores the titles of the table columns
     */
    private static final String[] COLUMNS = {"id", "title", "price", "attack_power"};
    /**
     * @value the field stores the title of the table
     */
    private static final String TABLE_NAME = "items";
    /**
     * @value field stores item's users
     */
    private ArrayList<User> users = new ArrayList<User>();
    /**
     * @value field stores item's encounters
     */
    private ArrayList<Encounter> encounters = new ArrayList<Encounter>();

    /**
     * method getId
     *
     * @return item id
     */
    public int getId() {
        return id;
    }

    /**
     * method getTitle
     *
     * @return item title
     */
    public String getTitle() {
        return title;
    }

    /**
     * method setTitle
     *
     * @param title item title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * method getPrice
     *
     * @return item price
     */
    public int getPrice() {
        return price;
    }

    /**
     * method setTitle
     *
     * @param price item price
     */
    public void setPrice(int price) {
        this.price = price;
    }

    /**
     * method getAttackPower
     *
     * @return item title
     */
    public int getAttack_power() {
        return attackPower;
    }

    /**
     * method setAttackPower
     *
     * @param attackPower item attackPower
     */
    public void setAttackPower(int attackPower) {
        this.attackPower = attackPower;
    }

    /**
     * method addUser
     * adds item's user
     *
     * @param user adds user
     */
    public void addUser(User user) {
        users.add(user);
    }

    /**
     * method getUsers
     *
     * @return item's users
     */
    public ArrayList<User> getUsers() {
        return users;
    }

    /**
     * method addEncounter
     * adds item's encounter
     *
     * @param encounter adds encounter
     */
    public void addEncounter(Encounter encounter) {
        encounters.add(encounter);
    }

    /**
     * method getEncounters
     *
     * @return item's encounters
     */
    public ArrayList<Encounter> getEncounters() {
        return encounters;
    }

    /**
     * Item constructor
     *
     * @param id          id item
     * @param title       title item
     * @param price       price item
     * @param attackPower attackPower
     */
    public Item(int id, String title, int price, int attackPower) {
        this.id = id;
        this.title = title;
        this.price = price;
        this.attackPower = attackPower;
    }

    /**
     * @value an instance of the BaseModel class
     */
    private static BaseModel<Item> bdConnection = new BaseModel<Item>();
    /**
     * lambda function that creates an instance of the Item class and returns it
     */
    public static BaseModel.classModel<Item> model = (resultSet) -> {
        Item itemDTO = null;
        try {

            int id = resultSet.getInt("id");
            String title = resultSet.getString("title");
            int price = resultSet.getInt("price");
            int attackPower = resultSet.getInt("attack_power");

            itemDTO = new Item(id, title, price, attackPower);


        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return itemDTO;
    };
    /**
     * lambda function that creates an instance of the Item class and returns it
     */
    private static BaseModel.classModel<Item> model2 = (resultSet) -> {
        Item itemDTO = null;
        try {
            int id = resultSet.getInt("id");
            String title = resultSet.getString("title");
            int price = resultSet.getInt("price");
            int attackPower = resultSet.getInt("attack_power");

            itemDTO = new Item(id, title, price, attackPower);

            itemDTO.addUser(User.getById(id));
            itemDTO.addEncounter(Encounter.getById(id));
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return itemDTO;
    };

    /**
     * executeWithResultSet method
     * execution with returning of the received data of the select
     *
     * @param query request
     * @return selection data array
     */
    public static ArrayList<Item> executeWithResultSet(String query) throws SQLException {
        return bdConnection.executeSelect(query, model);
    }

    /**
     * executeWithResultSet method
     * execution without returning of the received data of the select
     *
     * @param query request
     * @return selection data not null
     */
    public static boolean executeWithoutResultSet(String query, String title) throws SQLException {
        bdConnection.executeUpdate(query);
        return getByTitle(title) != null;
    }


    /**
     * static method getAll
     *
     * @return an array of all values in the table
     */
    public static ArrayList<Item> getAll() throws SQLException {
        String query = String.format("SELECT %s FROM %s", String.join(", ", COLUMNS), TABLE_NAME);
        return executeWithResultSet(query);
    }

    /**
     * static method getById
     *
     * @return Item with given id
     */
    public static Item getById(int id) throws SQLException {
        String query = String.format("SELECT %s FROM %s WHERE id = '%s'", String.join(", ", COLUMNS), TABLE_NAME, id);
        ArrayList<Item> item = executeWithResultSet(query);
        return item.get(0);
    }

    /**
     * static method getByTitle
     *
     * @return Item with given title
     */
    public static Item getByTitle(String title) throws SQLException {
        String query = String.format("SELECT %s FROM %s WHERE title = '%s'", String.join(", ", COLUMNS), TABLE_NAME, title);
        ArrayList<Item> item = executeWithResultSet(query);
        return item.get(0);
    }

    /**
     * static method getShopListRandom
     *
     * @return random Location
     */
    public static ArrayList<Item> getShopListRandom() throws SQLException {
        String query = String.format("SELECT %s FROM %s ORDER BY RANDOM() LIMIT 3",
                String.join(", ", COLUMNS), TABLE_NAME);
        return executeWithResultSet(query);
    }

    /**
     * static method create      *  new item
     *
     * @param title       title which we add to the database
     * @param price       price which we add to the database
     * @param attackPower attackPower which we add to the database
     */
    public static boolean create(String title, int price, int attackPower) throws SQLException {
        String query = String.format("insert into %s(title, price, attack_power) values('%s', '%s', '%s')",
                TABLE_NAME, title, price, attackPower);
        return executeWithoutResultSet(query, title);
    }

    /**
     * static method update     *  any record in the user's database
     *
     * @param title       title which we update in the database
     * @param price       price which we update in the database
     * @param attackPower attackPower which we update in the database
     */
    public static boolean update(String title, int price, int attackPower) throws SQLException {
        String query = String.format("update %s set price = '%s', attack_power = '%s' where title = '%s'",
                TABLE_NAME, price, attackPower, title);
        return executeWithoutResultSet(query, title);
    }

    /**
     * static method delete the existing item
     *
     * @param title item's title which we're deleting
     */
    public static void delete(String title) throws SQLException {
        String query = String.format("delete from %s where title = '%s'", TABLE_NAME, title);
        bdConnection.executeUpdate(query);
    }


}
