package newNovella.models;

import newNovella.BaseModel;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 * The AchievementToUser class is used to establish a connection with the database
 * and data storage
 *
 * @author Elizaveta_krasova, RudiFrost, Denchik89
 */

public class AchievementToUser {
    /**
     * @value the field stores the id of the achievementToUser
     */
    private int id;
    /**
     * @value the field stores the userId of the achievementToUser
     */
    private int userId;
    /**
     * @value the field stores the achievementId of the achievementToUser
     */
    private int achievementId;
    /**
     * @value the field stores the isEarned of the achievementToUser
     */
    private boolean isEarned;
    /**
     * an instance of the User class
     */
    private User user;
    /**
     * an instance of the Achievement class
     */
    private Achievement achievement;

    /**
     * method getId
     *
     * @return achievement id
     */

    public int getId() {
        return id;
    }

    /**
     * method setId
     *
     * @param id achievementToUser id
     */

    public void setId(int id) {
        this.id = id;
    }

    /**
     * method getUserId
     *
     * @return achievementToUser userId
     */
    public int getUserId() {
        return userId;
    }

    /**
     * method setUserId
     *
     * @param userId achievementToUser userId
     */
    public void setUserId(int userId) {
        this.userId = userId;
    }

    /**
     * method getAchievementId
     *
     * @return achievementToUser achievementId
     */
    public int getAchievementId() {
        return achievementId;
    }

    /**
     * method setAchievementId
     *
     * @param achievementId achievementToUser achievementId
     */
    public void setAchievementId(int achievementId) {
        this.achievementId = achievementId;
    }

    /**
     * method isEarned
     *
     * @return boolean isEarned
     */
    public boolean isEarned() {
        return isEarned;
    }

    /**
     * method setEarned
     *
     * @param earned achievementToUser isEarned
     */
    public void setEarned(boolean earned) {
        isEarned = earned;
    }

    /**
     * method getUser
     *
     * @return achievementToUser user
     */
    public User getUser() {
        return user;
    }

    /**
     * method setUser
     *
     * @param u achievementToUser user
     */
    public void setUser(User u) {
        this.user = u;
    }

    /**
     * method getAchievement
     *
     * @return achievementToUser achievement
     */
    public Achievement getAchievement() {
        return achievement;
    }

    /**
     * method setAchievement
     *
     * @param a achievementToUser achievement
     */
    public void setAchievement(Achievement a) {
        this.achievement = a;
    }

    /**
     * AchievementToUser constructor
     *
     * @param id          id achievementToUser
     * @param user        user achievementToUser
     * @param achievement achievement achievementToUser
     * @param isEarned    isEarned achievementToUser
     */
    public AchievementToUser(int id, User user, Achievement achievement, boolean isEarned) {
        this.id = id;
        this.user = user;
        this.achievement = achievement;
        this.isEarned = isEarned;
    }

    /**
     * @value the field stores the titles of the table columns
     */
    private static final String[] COLUMNS = {"id", "user_id", "achievement_id", "is_earned"};
    /**
     * @value the field stores the title of the table
     */
    private static final String TABLE_NAME = "achievements_to_users";
    /**
     * @value an instance of the BaseModel class
     */
    private static BaseModel<AchievementToUser> bdConnection = new BaseModel<AchievementToUser>();
    /**
     * lambda function that creates an instance of the AchievementToUser class and returns it
     */
    public static BaseModel.classModel<AchievementToUser> model = (resultSet) -> {
        AchievementToUser achievementToUser = null;
        try {
            int id = resultSet.getInt("id");
            int userId = resultSet.getInt("user_id");
            int achievementId = resultSet.getInt("achievement_id");
            boolean isEarned = resultSet.getBoolean("is_earned");


            achievementToUser = new AchievementToUser(
                    id,
                    User.getById(userId),
                    Achievement.getById(achievementId),
                    isEarned
            );
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return achievementToUser;
    };

    /**
     * executeWithResultSet method
     * execution with returning of the received data of the select
     *
     * @param query request
     * @return selection data array
     */
    public static ArrayList<AchievementToUser> executeWithResultSet(String query) throws SQLException {
        return bdConnection.executeSelect(query, model);
    }

    /**
     * executeWithResultSet method
     * execution without returning of the received data of the select
     *
     * @param query request
     * @return selection data not null
     */
    public static boolean executeWithoutResultSet(String query, int id) throws SQLException {
        bdConnection.executeUpdate(query);
        return getById(id) != null;
    }

    /**
     * static method getById
     *
     * @return AchievementToUser with given id
     */
    public static AchievementToUser getById(int id) throws SQLException {
        ArrayList<AchievementToUser> achievements = executeWithResultSet(String.format("SELECT %s FROM %s WHERE %s = '%s'",
                String.join(", ", COLUMNS), TABLE_NAME, COLUMNS[0], id));
        return achievements.isEmpty() ? null : achievements.get(0);
    }

    /**
     * static method getAll
     *
     * @return an array of all values in the table
     */
    public static ArrayList<AchievementToUser> getAll() throws SQLException {
        String query = String.format("SELECT %s FROM %s", String.join(", ", COLUMNS), TABLE_NAME);
        ArrayList<AchievementToUser> achievementToUser = executeWithResultSet(query);
        return achievementToUser.isEmpty() ? null : achievementToUser;
    }

    /**
     * static method create      *  new achievementToUser
     *
     * @param userId        code which we add to the database
     * @param achievementId title which we add to the database
     */
    public static void create(int userId, int achievementId) throws SQLException {
        String query = String.format("insert into %s (user_id, achievement_id) values('%s', '%s')",
                TABLE_NAME, userId, achievementId);
        bdConnection.executeUpdate(query);
    }

    /**
     * static method update     *  any record in the user's database
     *
     * @param id            id which we update in the database
     * @param userId        userId which we update in the database
     * @param achievementId achievementId which we update in the database
     */
    public static boolean update(int id, int userId, int achievementId) throws SQLException {
        String query = String.format("update %s set user_id = '%s', achievement_id = '%s', where id = '%s'",
                TABLE_NAME, userId, achievementId, id);
        return executeWithoutResultSet(query, id);
    }

    /**
     * static method delete the existing achievementToUser
     *
     * @param id achievement's code which we're deleting
     */
    public static void delete(int id) throws SQLException {
        bdConnection.executeUpdate(String.format("delete from %s where id = '%s'", TABLE_NAME, id));
    }


}
