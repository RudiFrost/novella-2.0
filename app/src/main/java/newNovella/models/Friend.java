package newNovella.models;

import newNovella.BaseModel;


import java.sql.SQLException;
import java.util.ArrayList;

/**
 * The Friend class is used to establish a connection with the database
 * and data storage
 *
 * @author Elizaveta_krasova, RudiFrost, Denchik89
 */
public class Friend extends BaseModel {
    /**
     * @value the field stores the id of the friend
     */
    private int id;
    /**
     * @value the field stores the name of the friend
     */
    private String name;
    /**
     * @value the field stores the healAmount of the friend
     */
    private int healAmount;
    /**
     * @value the field stores the title of the table
     */
    private static final String TABLE_NAME = "friends";
    /**
     * @value the field stores the titles of the table columns
     */
    private static final String[] COLUMNS = {"id", "name", "heal_amount"};
    /**
     * method getId
     *
     * @return friend id
     */
    public int getId() {
        return id;
    }
    /**
     * method getName
     *
     * @return friend name
     */
    public String getName() {
        return name;
    }
    /**
     * method setName
     *
     * @param name friend name
     */
    public void setName(String name) {
        this.name = name;
    }
    /**
     * method getHealAmount
     *
     * @return friend healAmount
     */
    public int getHeal_amount() {
        return healAmount;
    }
    /**
     * method setHealAmount
     *
     * @param healAmount friend healAmount
     */
    public void setHealAmount(int healAmount) {
        this.healAmount = healAmount;
    }
    /**
     * Friend constructor
     *
     * @param id    id friend
     * @param name  name friend
     * @param healAmount healAmount friend
     */
    public Friend(int id, String name, int healAmount) {
        this.id = id;
        this.name = name;
        this.healAmount = healAmount;
    }

    public Friend() {

    }

    /**
     * Friend constructor
     *
     * @param name  name friend
     * @param healAmount healAmount friend
     */
    public Friend(String name, int healAmount) {
        this.name = name;
        this.healAmount = healAmount;
    }
    /**
     * @value an instance of the BaseModel class
     */
    private static BaseModel<Friend> bdConnection = new BaseModel<Friend>();
    /**
     * lambda function that creates an instance of the Friend class and returns it
     */
    public static BaseModel.classModel<Friend> model = (resultSet) -> {
        Friend friend = null;
        try{
            friend = new Friend(
                    resultSet.getInt(COLUMNS[0]),
                    resultSet.getString(COLUMNS[1]),
                    resultSet.getInt(COLUMNS[2])
            );
        }
        catch (SQLException e ) {System.out.println(e.getMessage());}
        return friend;
    };


    /**
     * executeWithResultSet method
     * execution with returning of the received data of the select
     *
     * @param query request
     * @return selection data array
     */
    public static ArrayList<Friend> executeWithResultSet(String query) throws SQLException{
        return bdConnection.executeSelect(query, model);
    }
    /**
     * executeWithResultSet method
     * execution without returning of the received data of the select
     *
     * @param query request
     * @return selection data not null
     */
    public static boolean executeWithoutResultSet(String query, String name) throws SQLException{
        bdConnection.executeUpdate(query);
        return getByName(name) != null;
    }

    /**
     * static method getAll
     *
     * @return an array of all values in the table
     */
    public static ArrayList<Friend> getAll() throws SQLException {
        String query = String.format("SELECT %s FROM %s", String.join(", ", COLUMNS), TABLE_NAME);
        return executeWithResultSet(query);
    }
    /**
     * static method getByName
     *
     * @return Friend with given name
     */
    public static Friend getByName(String name) throws SQLException {
        String query = String.format("SELECT %s FROM %s WHERE %s = '%s'",
                String.join(", ", COLUMNS), TABLE_NAME, COLUMNS[1], name);
        ArrayList<Friend> friends = executeWithResultSet(query);
        return friends.get(0);
    }
    /**
     * static method getById
     *
     * @return Friend with given id
     */
    public static Friend getById(int id) throws SQLException {
        String query = String.format("SELECT %s FROM %s WHERE %s = '%s'",
                String.join(", ", COLUMNS), TABLE_NAME, COLUMNS[0], id);
        ArrayList<Friend> friends = executeWithResultSet(query);
        return friends.get(0);
    }

    public static Friend getFriendRandom() throws SQLException {
        String query = String.format("SELECT %s FROM %s ORDER BY RANDOM() LIMIT 1",
                String.join(", ", COLUMNS), TABLE_NAME);
        ArrayList<Friend> friends = executeWithResultSet(query);
        return friends.get(0);
    }

    /**
     * static method create      *  new friend
     *
     * @param name  name which we add to the database
     * @param healAmount healAmount which we add to the database
     */
    public static boolean create(String name, int healAmount) throws SQLException {
        String query = String.format("insert into %s (%s, %s) values('%s', '%s')",
                TABLE_NAME, COLUMNS[1], COLUMNS[2], name, healAmount);
        return executeWithoutResultSet(query, name);
    }
    /**
     * static method update     *  any record in the user's database
     *
     * @param name  name which we update in the database
     * @param healAmount healAmount which we update in the database
     */
    public static boolean update(String name, int healAmount) throws SQLException {
        String query = String.format("update %s set %s = '%s' where %s = '%s'",
                TABLE_NAME, COLUMNS[2], healAmount, COLUMNS[1], name);
        return executeWithoutResultSet(query, name);
    }
    /**
     * static method delete the existing friend
     *
     * @param name friend's name which we're deleting
     */
    public static void delete(String name) throws SQLException {
        String query = String.format("delete from %s where %s = '%s'", TABLE_NAME, COLUMNS[1], name);
        bdConnection.executeUpdate(query);
    }



}