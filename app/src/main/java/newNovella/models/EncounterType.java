package newNovella.models;

import newNovella.BaseModel;


import java.sql.SQLException;
import java.util.ArrayList;

/**
 * The EncounterType class is used to establish a connection with the database
 * and data storage
 *
 * @author Elizaveta_krasova, RudiFrost, Denchik89
 */
public class EncounterType extends BaseModel{
    /**
     * @value the field stores the code of the encounterType
     */
    private String code;
    /**
     * @value the field stores the name of the encounterType
     */
    private String name;
    /**
     * @value the field stores the id of the encounterType
     */
    private int id;
    /**
     * @value the field stores the title of the table
     */
    private static final String TABLE_NAME = "encounter_types";
    /**
     * @value the field stores the titles of the table columns
     */
    private static final String[] COLUMNS = {"id", "code", "name"};
    /**
     * method getCode
     *
     * @return encounterType code
     */
    public String getCode() {
        return code;
    }
    /**
     * method setCode
     *
     * @param code encounterType code
     */
    public void setCode(String code) {
        this.code = code;
    }
    /**
     * method getName
     *
     * @return encounterType name
     */
    public String getName() {
        return name;
    }
    /**
     * method setName
     *
     * @param name encounterType name
     */
    public void setName(String name) {
        this.name = name;
    }
    /**
     * method getId
     *
     * @return encounterType id
     */
    public int getId() {
        return id;
    }
    /**
     * EncounterType constructor
     *
     * @param id    id encounterType
     * @param code  code encounterType
     * @param name title encounterType
     */
    public EncounterType(int id, String code, String name) {
        this.id = id;
        this.code = code;
        this.name = name;
    }
    /**
     * EncounterType constructor
     *
     * @param name title encounterType
     */
    public EncounterType(String name) {
        if (code.isEmpty()) {
            this.name = name;
        }
    }
    /**
     * @value an instance of the BaseModel class
     */
    private static BaseModel<EncounterType> bdConnection = new BaseModel<EncounterType>();
    /**
     * lambda function that creates an instance of the EncounterType class and returns it
     */
    public static BaseModel.classModel<EncounterType> model = (resultSet) -> {
        EncounterType encounterType = null;
        try{
            int id = resultSet.getInt(COLUMNS[0]);
            String code = resultSet.getString(COLUMNS[1]);
            String name = resultSet.getString(COLUMNS[2]);
            if (code.isEmpty()) {
                encounterType = new EncounterType(name);
            } else {
                encounterType = new EncounterType(id, code, name);
            }

        }
        catch (SQLException e ) {System.out.println(e.getMessage());}
        return encounterType;
    };

    /**
     * executeWithResultSet method
     * execution with returning of the received data of the select
     *
     * @param query request
     * @return selection data array
     */
    public static ArrayList<EncounterType> executeWithResultSet(String query) throws SQLException{
        return bdConnection.executeSelect(query, model);
    }
    /**
     * executeWithResultSet method
     * execution without returning of the received data of the select
     *
     * @param query request
     * @return selection data not null
     */
    public static boolean executeWithoutResultSet(String query, String code) throws SQLException{
        bdConnection.executeUpdate(query);
        return getByCode(code) != null;
    }

    /**
     * static method getAll
     *
     * @return an array of all values in the table
     */
    public static ArrayList<EncounterType> getAll() throws SQLException {
        String query = String.format("SELECT %s FROM %s", String.join(", ", COLUMNS), TABLE_NAME);
        return executeWithResultSet(query);
    }
    /**
     * static method getByCode
     *
     * @return EncounterType with given code
     */
    public static EncounterType getByCode(String code) throws SQLException {
        String query = String.format("SELECT %s FROM %s WHERE %s = '%s'", String.join(", ", COLUMNS), TABLE_NAME, COLUMNS[1], code);
        ArrayList<EncounterType> encounterTypes = executeWithResultSet(query);
        return encounterTypes.get(0);
    }
    /**
     * static method getById
     *
     * @return EncounterType with given id
     */
    public static EncounterType getById(int id) throws SQLException {
        String query = String.format("SELECT %s FROM %s WHERE %s = '%s'", String.join(", ", COLUMNS), TABLE_NAME, COLUMNS[0], id);
        ArrayList<EncounterType> encounterTypes = executeWithResultSet(query);
        return encounterTypes.get(0);
    }

    /**
     * static method getEncounterTypesRandom
     *
     * @return random EncounterType
     */
    public static EncounterType getEncounterTypesRandom() throws SQLException {
        String query = String.format("SELECT %s FROM %s ORDER BY RANDOM() LIMIT 1", String.join(", ", COLUMNS), TABLE_NAME);
        ArrayList<EncounterType> encounterTypes = executeWithResultSet(query);
        return encounterTypes.get(0);
    }
    /**
     * static method create      *  new encounterType
     *
     * @param code  code which we add to the database
     * @param name name which we add to the database
     */
    public static boolean create(String code, String name) throws SQLException {
        String query = String.format("insert into %s (%s, %s) values('%s', '%s')",
                TABLE_NAME, COLUMNS[1], COLUMNS[2], code, name);
        return executeWithoutResultSet(query, code);
    }
    /**
     * static method update     *  any record in the user's database
     *
     * @param code  code which we update in the database
     * @param name name which we update in the database
     */
    public static boolean update(String code, String name) throws SQLException {
        String query = String.format("update %s set %s = '%s' where %s = '%s'",
                TABLE_NAME, COLUMNS[2], name, COLUMNS[1], code);
        return executeWithoutResultSet(query, code);
    }
    /**
     * static method delete the existing encounterType
     *
     * @param code encounterType's code which we're deleting
     */
    public static void delete(String code) throws SQLException {
        String query = String.format("delete from %s where %s = '%s'", TABLE_NAME, COLUMNS[1], code);
        bdConnection.executeUpdate(query);
    }




}
