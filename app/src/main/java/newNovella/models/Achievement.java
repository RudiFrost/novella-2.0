package newNovella.models;

import java.sql.SQLException;
import java.util.ArrayList;

import newNovella.BaseModel;

/**
 * The Achievement class is used to establish a connection with the database
 * and data storage
 *
 * @author Elizaveta_krasova, RudiFrost, Denchik89
 */

public class Achievement extends BaseModel {
    /**
     * @value the field stores the id of the achievement
     */
    private int id;
    /**
     * @value the field stores the code of the achievement
     */
    private String code;
    /**
     * @value the field stores the title of the achievement
     */
    private String title;
    /**
     * @value field stores achievement's users
     */
    private ArrayList<User> users = new ArrayList<>();
    /**
     * @value the field stores the titles of the table columns
     */
    private static final String[] COLUMNS = {"id", "code", "title"};
    /**
     * @value the field stores the title of the table
     */
    private static final String TABLE_NAME = "achievements";
    /**
     * @value an instance of the BaseModel class
     */
    private static BaseModel<Achievement> conn = new BaseModel<>();

    /**
     * lambda function that creates an instance of the Achievement class and returns it
     */
    public static BaseModel.classModel<Achievement> achievementLambda = (rs) ->
    {
        Achievement achievement = null;
        try {
            achievement = new Achievement(
                    rs.getInt(COLUMNS[0]),
                    rs.getString(COLUMNS[1]),
                    rs.getString(COLUMNS[2]));
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return achievement;
    };

    /**
     * empty Achievement constructor
     */
    Achievement() {
    }

    /**
     * Achievement constructor
     *
     * @param id    id achievement
     * @param code  code achievement
     * @param title title achievement
     */
    public Achievement(int id, String code, String title) {
        this.id = id;
        this.code = code;
        this.title = title;
    }

    /**
     * method getId
     *
     * @return achievement id
     */

    public int getId() {
        return id;
    }

    /**
     * method setCode
     *
     * @param code achievement code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * method getCode
     *
     * @return achievement code
     */
    public String getCode() {
        return code;
    }

    /**
     * method setTitle
     *
     * @param title achievement title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * method getTitle
     *
     * @return achievement title
     */
    public String getTitle() {
        return title;
    }

    /**
     * method addUser
     * adds achievement's user
     * @param us adds user
     */
    public void addUser(User us) {
        users.add(us);
    }

    /**
     * method getUsers
     *
     * @return achievement's users
     */
    public ArrayList<User> getUsers() {
        return users;
    }

    /**
     * executeWithResultSet method
     * execution with returning of the received data of the select
     *
     * @param query request
     * @return selection data array
     */

    public static ArrayList<Achievement> executeWithResultSet(String query) throws SQLException {
        return conn.executeSelect(query, achievementLambda);
    }

    /**
     * executeWithResultSet method
     * execution without returning of the received data of the select
     *
     * @param query request
     * @return selection data not null
     */
    public static boolean executeWithoutResultSet(String query, String code) throws SQLException {
        conn.executeUpdate(query);
        return getByCode(code) != null;
    }

    /**
     * static method getAll
     *
     * @return an array of all values in the table
     */

    public static ArrayList<Achievement> getAll() throws SQLException {
        return executeWithResultSet(String.format("SELECT %s FROM %s", String.join(", ", COLUMNS), TABLE_NAME));
    }

    /**
     * static method getById
     *
     * @return Achievement with given id
     */
    public static Achievement getById(int id) throws SQLException {
        ArrayList<Achievement> ach = executeWithResultSet(String.format("SELECT %s FROM %s WHERE %s in ('%s')",
                String.join(", ", COLUMNS), TABLE_NAME, COLUMNS[0], id));
        return ach.isEmpty() ? null : ach.get(0);
    }

    /**
     * static method getByCode
     *
     * @return Achievement with given code
     */
    public static Achievement getByCode(String code) throws SQLException {
        ArrayList<Achievement> ach = executeWithResultSet(String.format("SELECT %s FROM %s WHERE %s in ('%s')",
                String.join(", ", COLUMNS), TABLE_NAME, COLUMNS[1], code));
        return ach.isEmpty() ? null : ach.get(0);
    }

    /**
     * static method create      *  new achievement
     *
     * @param code  code which we add to the database
     * @param title title which we add to the database
     */

    public static boolean create(String code, String title) throws SQLException {
        return executeWithoutResultSet(String.format("INSERT INTO %s (%s, %s) VALUES('%s', '%s')",
                TABLE_NAME, COLUMNS[1], COLUMNS[2], code, title), code);
    }

    /**
     * static method delete the existing achievement
     *
     * @param code achievement's code which we're deleting
     */

    public static void delete(String code) throws SQLException {
        executeUpdate(String.format("DELETE FROM %s WHERE %s in ('%s')", TABLE_NAME, COLUMNS[1], code));
    }

    /**
     * static method update     *  any record in the user's database
     *
     * @param code  code which we update in the database
     * @param title title which we update in the database
     */

    public static boolean update(String code, String title) throws SQLException {
        return executeWithoutResultSet(String.format("UPDATE %s SET %s = '%s', %s = '%s' WHERE %s = '%s'",
                TABLE_NAME, COLUMNS[1], code, COLUMNS[2], title, COLUMNS[1], code), code);
    }

    /**
     * @return achievement as string
     */
    public String toString() {
        return String.format("%s  %s   %s", this.id, this.code, this.title);
    }
}
