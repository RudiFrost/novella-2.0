package newNovella.models;

import java.sql.SQLException;
import java.util.ArrayList;

import newNovella.BaseModel;

/**
 * The User class is used to establish a connection with the database
 * and data storage
 *
 * @author Elizaveta_krasova, RudiFrost, Denchik89
 */
public class User extends BaseModel {
    /**
     * @value the field stores the id of the User
     */
    private int id;
    /**
     * @value the field stores the name of the User
     */
    private String name;
    /**
     * @value the field stores the money of the User
     */
    private static int money = 100;
    /**
     * @value the field stores the hp of the User
     */
    private int hp;
    /**
     * @value the field stores the attack of the User
     */
    private static int attack = 25;
    /**
     * @value the field stores the HP of the User
     */
    private final static int HP = 100;
    /**
     * @value field stores user's achievement
     */
    private ArrayList<Achievement> achievements = new ArrayList<>();
    /**
     * @value field stores user's items
     */
    private ArrayList<Item> items = new ArrayList<>();
    /**
     * @value the field stores the title of the table
     */
    private static final String TABLE_NAME = "users";
    /**
     * @value the field stores the titles of the table columns
     */
    private static final String[] COLUMNS = {"id", "name", "money", "attack", "hp"};
    /**
     * @value an instance of the BaseModel class
     */
    private static BaseModel<User> conn = new BaseModel<>();

    /**
     * lambda function that creates an instance of the User class and returns it
     */
    public static BaseModel.classModel<User> userLambda = (rs) ->
    {
        User user = null;
        try {
            user = new User(
                    rs.getInt(COLUMNS[0]),
                    rs.getString(COLUMNS[1]),
                    rs.getInt(COLUMNS[2]),
                    rs.getInt(COLUMNS[3]),
                    rs.getInt(COLUMNS[4]));
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return user;
    };


    User() {
    }

    /**
     * Achievement constructor
     *
     * @param id     id user
     * @param name   name user
     * @param money  money user
     * @param attack attack user
     * @param hp     hp user
     */
    public User(int id, String name, int money, int attack, int hp) {
        this.id = id;
        this.name = name;
        User.money = money;
        User.attack = attack;
        this.hp = hp;
    }

    /**
     * method getId
     *
     * @return user id
     */
    public int getId() {
        return id;
    }

    /**
     * method setName
     *
     * @param name user's name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * method getName
     *
     * @return user's name
     */

    public String getName() {
        return name;
    }

    /**
     * method getAttack
     *
     * @return user's attack
     */

    public static int getAttack() {
        return User.attack;
    }

    /**
     * method setAttack
     *
     * @param attack user's attack
     */
    public static void setAttack(int attack) {
        User.attack = attack;
    }

    /**
     * method addAttack
     *
     * @param attack user's attack
     */
    public static void addAttack(int attack) {
        User.attack += attack;
    }

    /**
     * method setMoney
     * adds user's attack
     *
     * @param money user's money
     */
    public static void setMoney(int money) {
        User.money = money;
    }

    /**
     * method addMoney
     * adds user's money
     *
     * @param money user's money
     */
    public static void addMoney(int money) {
        User.money += money;
    }

    /**
     * method removeMoney
     * remove user's money
     *
     * @param money user's money
     */
    public static void removeMoney(int money) {
        User.money -= money;
    }

    /**
     * method getMoney
     *
     * @return user's money
     */
    public static int getMoney() {
        return User.money;
    }

    /**
     * method getHp
     *
     * @return user hp
     */
    public int getHp() {
        return hp;
    }

    /**
     * method setHp
     *
     * @param hp user hp
     */
    public void setHp(int hp) {
        this.hp = hp;
    }

    /**
     * method addAchievement
     * adds user's achievement
     *
     * @param ach user's achievements
     */
    public void addAchievement(Achievement ach) {
        achievements.add(ach);
    }

    /**
     * method getAchievements
     *
     * @return user's achievements
     */
    public ArrayList<Achievement> getAchievements() {
        return achievements;
    }

    /**
     * method addItem
     * adds user's item
     *
     * @param it user's items
     */
    public void addItem(Item it) {
        items.add(it);
    }

    /**
     * method getItems
     *
     * @return user's items
     */
    public ArrayList<Item> getItems() {
        return items;
    }

    /**
     * executeWithResultSet method
     * execution with returning of the received data of the select
     *
     * @param query request
     * @return selection data array
     */
    public static ArrayList<User> executeWithResultSet(String query) throws SQLException {
        return conn.executeSelect(query, userLambda);
    }

    /**
     * executeWithResultSet method
     * execution without returning of the received data of the select
     *
     * @param query request
     * @return selection data not null
     */
    public static boolean executeWithoutResultSet(String query, String name) throws SQLException {
        conn.executeUpdate(query);
        return getByName(name) != null;
    }

    /**
     * static method getAll
     *
     * @return an array of all values in the table
     */
    public static ArrayList<User> getAll() throws SQLException {
        return executeWithResultSet(String.format("SELECT %s FROM %s", String.join(", ", COLUMNS), TABLE_NAME));
    }

    /**
     * static method getById
     *
     * @return User with given id
     */
    public static User getById(int id) throws SQLException {
        ArrayList<User> us = executeWithResultSet(String.format("SELECT %s FROM %s WHERE id = '%s';",
                String.join(", ", COLUMNS), TABLE_NAME, id));
        return us.isEmpty() ? null : us.get(0);
    }

    /**
     * static method getById
     *
     * @return User with given name
     */
    public static User getByName(String name) throws SQLException {
        ArrayList<User> us = executeWithResultSet(String.format("SELECT %s FROM %s WHERE name = '%s';",
                String.join(", ", COLUMNS), TABLE_NAME, name));
        return us.isEmpty() ? null : us.get(0);
    }

    /**
     * static method create      *  new user
     *
     * @param name name which we add to the database
     */
    public static boolean create(String name) throws SQLException {
        return executeWithoutResultSet(String.format("INSERT INTO %s (%s, %s, %s, %s) VALUES('%s', '%s', '%s', '%s')",
                TABLE_NAME, COLUMNS[1], COLUMNS[2], COLUMNS[3], COLUMNS[4], name, money, attack, HP), name);
    }

    /**
     * static method delete the existing achievement
     * updating any record in the user's database
     *
     * @param name user's name which we're deleting
     */
    public static void delete(String name) throws SQLException {
        conn.executeUpdate(String.format("DELETE FROM %s WHERE %s in ('%s')",
                TABLE_NAME, COLUMNS[1], name));
    }

    /**
     * static method update     *  any record in the user's database
     *
     * @param name   name which we update in the database
     * @param money  money which we update in the database
     * @param attack attack which we update in the database
     * @param hp     hp which we update in the database
     */
    public static boolean update(String name, int money, int attack, int hp) throws SQLException {
        return executeWithoutResultSet(String.format("UPDATE %s SET %s = '%s',  %s = '%s', %s = '%s' WHERE %s = '%s'",
                TABLE_NAME, COLUMNS[2], money, COLUMNS[3], attack, COLUMNS[4], hp, COLUMNS[1], name), name);
    }

    public static boolean updateMoney(String name, int money) throws SQLException {
        return executeWithoutResultSet(String.format("UPDATE %s SET %s = '%s' WHERE %s = '%s'",
                TABLE_NAME, COLUMNS[2], money, COLUMNS[1], name), name);
    }

    public static boolean updateAttack(String name, int attack) throws SQLException {
        return executeWithoutResultSet(String.format("UPDATE %s SET %s = '%s' WHERE %s = '%s'",
                TABLE_NAME, COLUMNS[3], attack, COLUMNS[1], name), name);
    }

    public static boolean updateHp(String name, int hp) throws SQLException {
        return executeWithoutResultSet(String.format("UPDATE %s SET %s = '%s' WHERE %s = '%s'",
                TABLE_NAME, COLUMNS[4], hp, COLUMNS[1], name), name);
    }


    /**
     * static method updateMoney     *  any record in the user's database
     *
     * @param name   name which we update in the database
     * @param money  money which we update in the database
     */
    public static boolean updateMoney(String name, int money) throws SQLException {
        return executeWithoutResultSet(String.format("UPDATE %s SET %s = '%s' WHERE %s = '%s'",
                TABLE_NAME, COLUMNS[2], money, COLUMNS[1], name), name);
    }

    /**
     * static method updateAttack     *  any record in the user's database
     *
     * @param name   name which we update in the database
     * @param attack attack which we update in the database
     */
    public static boolean updateAttack(String name, int attack) throws SQLException {
        return executeWithoutResultSet(String.format("UPDATE %s SET %s = '%s' WHERE %s = '%s'",
                TABLE_NAME, COLUMNS[3], attack, COLUMNS[1], name), name);
    }


    /**
     * static method updateHp     *  any record in the user's database
     * @param name   name which we update in the database
     * @param hp     hp which we update in the database
     */
    public static boolean updateHp(String name, int hp) throws SQLException {
        return executeWithoutResultSet(String.format("UPDATE %s SET %s = '%s' WHERE %s = '%s'",
                TABLE_NAME, COLUMNS[4], hp, COLUMNS[1], name), name);
    }

    /**
     * @return user as string
     */
    public String toString() {
        return String.format("%s  %s   %s  %s", this.id, this.name, this.attack, this.money);
    }
}
