package newNovella.models;

import newNovella.BaseModel;


import java.sql.SQLException;
import java.util.ArrayList;

/**
 * The Location class is used to establish a connection with the database
 * and data storage
 *
 * @author Elizaveta_krasova, RudiFrost, Denchik89
 */
public class Location extends BaseModel {
    /**
     * @value the field stores the id of the Location
     */
    private int id;
    /**
     * @value the field stores the code of the Location
     */
    private String code;
    /**
     * @value the field stores the place of the Location
     */
    private String place;
    /**
     * @value the field stores the title of the table
     */
    private static final String TABLE_NAME = "locations";
    /**
     * @value the field stores the titles of the table columns
     */
    private static final String[] COLUMNS = {"id", "code", "place"};

    /**
     * method getCode
     *
     * @return location code
     */
    public String getCode() {
        return code;
    }

    /**
     * method setCode
     *
     * @param code location code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * method getPlace
     *
     * @return location place
     */
    public String getPlace() {
        return place;
    }

    /**
     * method setPlace
     *
     * @param place location place
     */
    public void setPlace(String place) {
        this.place = place;
    }

    /**
     * method getId
     *
     * @return location id
     */
    public int getId() {
        return id;
    }

    /**
     * Location constructor
     *
     * @param id    id location
     * @param code  code location
     * @param place title location
     */
    public Location(int id, String code, String place) {
        this.id = id;
        this.code = code;
        this.place = place;
    }

    /**
     * Location constructor
     *
     * @param place title location
     */
    public Location(String place) {
        if (code.isEmpty()) {
            this.place = place;
        }
    }

    /**
     * @value an instance of the BaseModel class
     */
    private static BaseModel<Location> bdConnection = new BaseModel<Location>();
    /**
     * lambda function that creates an instance of the Location class and returns it
     */
    public static BaseModel.classModel<Location> model = (resultSet) -> {
        Location location = null;
        try {
            int id = resultSet.getInt(COLUMNS[0]);
            String code = resultSet.getString(COLUMNS[1]);
            String place = resultSet.getString(COLUMNS[2]);

            if (code.isEmpty()) {
                location = new Location(place);
            } else {
                location = new Location(id, code, place);
            }

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return location;
    };

    /**
     * executeWithResultSet method
     * execution with returning of the received data of the select
     *
     * @param query request
     * @return selection data array
     */
    public static ArrayList<Location> executeWithResultSet(String query) throws SQLException {
        return bdConnection.executeSelect(query, model);
    }

    /**
     * executeWithResultSet method
     * execution without returning of the received data of the select
     *
     * @param query request
     * @return selection data not null
     */
    public static boolean executeWithoutResultSet(String query, String code) throws SQLException {
        bdConnection.executeUpdate(query);
        return getByCode(code) != null;
    }

    /**
     * static method getAll
     *
     * @return an array of all values in the table
     */
    public static ArrayList<Location> getAll() throws SQLException {
        String query = String.format("SELECT %s FROM %s", String.join(", ", COLUMNS), TABLE_NAME);
        return executeWithResultSet(query);
    }

    /**
     * static method getByCode
     *
     * @return Location with given code
     */
    public static Location getByCode(String code) throws SQLException {
        String query = String.format("SELECT %s FROM %s WHERE %s = '%s'", String.join(", ", COLUMNS), TABLE_NAME, COLUMNS[1], code);
        ArrayList<Location> locations = executeWithResultSet(query);
        return locations.get(0);
    }

    /**
     * static method getById
     *
     * @return Location with given id
     */
    public static Location getById(int id) throws SQLException {
        String query = String.format("SELECT %s FROM %s WHERE %s = '%s'", String.join(", ", COLUMNS), TABLE_NAME, COLUMNS[0], id);
        ArrayList<Location> locations = executeWithResultSet(query);
        return locations.get(0);
    }

    /**
     * static method getLocationsRandom
     *
     * @return random Location
     */
    public static Location getLocationsRandom() throws SQLException {
        String query = String.format("SELECT %s FROM %s ORDER BY RANDOM() LIMIT 1", String.join(", ", COLUMNS), TABLE_NAME);
        ArrayList<Location> locations = executeWithResultSet(query);
        return locations.get(0);
    }

    /**
     * static method create      *  new location
     *
     * @param code  code which we add to the database
     * @param place title which we add to the database
     */
    public static boolean create(String code, String place) throws SQLException {
        String query = String.format("insert into %s (%s, %s) values('%s', '%s')", TABLE_NAME, COLUMNS[1], COLUMNS[2], code, place);
        return executeWithoutResultSet(query, code);
    }

    /**
     * static method update     *  any record in the user's database
     *
     * @param code  code which we update in the database
     * @param place title which we update in the database
     */
    public static boolean update(String code, String place) throws SQLException {
        String query = String.format("update %s set %s = '%s' where %s = '%s'", TABLE_NAME, COLUMNS[2], place, COLUMNS[1], code);
        return executeWithoutResultSet(query, code);
    }

    /**
     * static method delete the existing location
     *
     * @param code location's code which we're deleting
     */
    public static void delete(String code) throws SQLException {
        String query = String.format("delete from %s where %s = '%s'", TABLE_NAME, COLUMNS[1], code);
        bdConnection.executeUpdate(query);
    }


}
