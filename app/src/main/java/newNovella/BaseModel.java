package newNovella;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * The BaseConnection class is used to manage databases through other classes
 *
 * @author Denchik89
 */


public class BaseModel<T> {
    /**
     * @value field of the Connection object
     */
    private static Connection connection;

    /**
     * Generalized interface for lambda
     */

    public interface classModel<E> {
        E newModel(ResultSet resultSet);
    }

    /**
     * @value field to store name of the database
     */
    private static final String USERNAME = "postgres";
    /**
     * @value field to store connection password
     */
    private static final String PASSWORD = "mysecretpassword";
    /**
     * @value field to store database URL
     */
    private static final String URL = "jdbc:postgresql://127.0.0.1:5432/novella2.0";

    /**
     * createConnection method
     * creates a database connection
     *
     * @return field of the Connection object
     */

    public static Connection createConnection() {
        String username = USERNAME;
        String password = PASSWORD;
        String url = URL;
        Connection connection = null;
        try {
            connection = DriverManager.getConnection(url, username, password);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return connection;
    }

    /**
     * createConnection method
     * checks the status of the connection field
     *
     * @return field of the Connection object
     */

    public static Connection getConnection() {
        if (connection == null) {
            connection = createConnection();
        }
        return connection;
    }

    /**
     * executeSelect method
     * receives data from the database and writes to the fields of the class
     *
     * @param query request
     * @param lambda lambda
     * @return array of an instance of the ArrayList class
     */

    public ArrayList<T> executeSelect(String query, classModel<T> lambda) {
        ArrayList<T> model = new ArrayList<T>();


        try {
            Statement statement = getConnection().createStatement();
            ResultSet resultSet = statement.executeQuery(query);

            while (resultSet.next()) {
                model.add(lambda.newModel(resultSet));
            }

            if (statement != null) {
                statement.close();
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return model;
    }

    /**
     * method executeUpdate
     * * sets an update request in the database
     *
     * @param query request
     */

    public static void executeUpdate(String query) {
        try {
            Statement statement = getConnection().createStatement();
            statement.executeUpdate(query);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * method executeInsert
     * sets a request to insert to the database
     *
     * @param query request
     */

    public static void executeInsert(String query) {
        try {
            Statement statement = getConnection().createStatement();
            statement.execute(query);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

}

