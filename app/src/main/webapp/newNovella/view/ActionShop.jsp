<%@ page import="static newNovella.encounters.empty.EmptyProcess.itemList" %>
<%@ page import="newNovella.models.User" %>
<%@ page import="newNovella.models.Item" %>
<%@ page import="newNovella.State" %>
<%@ page import="java.util.ArrayList"%>
<%@ page contentType="text/html; charset=UTF-8" language="java" %>

<html>
    <body bgcolor="#6495ED" align="center">
        <p>перед вами вывешен список товаров которые вы можете приобрести</p>
        <% for (int i = 0; i < itemList.size(); i++) { %>
            <form action="/newNovella/by_in_shop/<%= i + 1%>" method="post">
                <input type='submit' value='<%= i + 1 + ") " + itemList.get(i).getTitle() + " " + itemList.get(i).getAttack_power() + " урона " + itemList.get(i).getPrice() + " монет" + "\n" %>'>
            </form>
        <% } %>

        <%= "Ваш баланс: " + User.getByName(State.name).getMoney() %>
    </body>

<form method="get" action="/newNovella/main" align="left">
    <button type="submit">Вернуться на главную</button>
</form>

</html>