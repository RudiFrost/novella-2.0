<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<html>
    <body bgcolor="#ff2400" align="center">
        <h1>
            <% String message = (String) request.getAttribute("message"); %>
            <%= message %>
        </h1>

        <form method="get" action="/newNovella/main">
            <button type="submit">Вернуться на главную</button>
        </form>

        <% String nameImage = "image/image" + ((int)(Math.random() * 7) + 1)+ ".jpg";%>
        <img src=<%=nameImage%>>
    </body>
</html>