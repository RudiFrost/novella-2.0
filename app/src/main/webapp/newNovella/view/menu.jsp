<%@ page import="newNovella.State" %>
<%@ page import="newNovella.Node" %>
<%@ page import="newNovella.encounters.Encount" %>
<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<% Node node = (Node) request.getAttribute("node"); %>
<% Encount encount = node.getEncount(); %>
<html>
    <body bgcolor="#6495ED" align="center">
        <h1>Игра началась</h1>

        <p><%= newNovella.State.name %>, вы попали на локацию <%= newNovella.State.location.getPlace() %>
        <p><%= encount.getMessage() %></p>

        <% for (int i = 0; i < encount.actions.size(); i++) { %>
            <form action="/newNovella/node_action_dispatcher/<%= i %>" method="post">
                <input type='submit' value='<%= encount.actions.get(i) %>'>
            </form>
        <% } %>
    </body>

<form method="get" action="/newNovella/main" align="left">
    <button type="submit">Вернуться на главную</button>
</form>

</html>
