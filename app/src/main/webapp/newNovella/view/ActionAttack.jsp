<%@ page import="static newNovella.encounters.enemy.EnemyProcess.enemyStats" %>
<%@ page import="newNovella.State" %>
<%@ page import="newNovella.models.User" %>
<%@ page import="newNovella.models.Enemy" %>
<%@ page import="newNovella.encounters.enemy.EnemyProcess" %>
<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<html>
    <body bgcolor="#6495ED" align="center">
        <h1>Привет, я <%= enemyStats.getName() %></h1>
        <% User user = User.getByName(State.name); %>
        <% Enemy enemy = EnemyProcess.enemyStats; %>

        <% if (State.hpEnemyBefore > 0 && State.hpUserBefore > 0) { %>
            <% if (!State.flagRandomLuck) { %>
                <p>Вы нанесли врагу <%= user.getAttack() %> урона</p>
                <p>Враг нанес вам <%= enemy.getAttack_power() %> урона</p>
                <p>У вас осталось <%= State.hpUserAfter  %> Hp</p>
            <% } else { %>
                <p>Вы нанесли врагу  <%= user.getAttack() %>  урона</p>
                <p>Вы уклонились от атаки противника!</p>
            <% } %>
        <% } %>
        <p>У врага осталось <%= enemy.getHp() %> Hp</p>
        <% if (State.hpUserAfter <= 0) {%>
            <p>Вы проиграли</p>
            <p>Ваша игра закончилась. Если хотите пройти ещё раз, тыкни сюда</p>
            <form method="get" action="/newNovella/main">
                <button type="submit">Пройти заново</button>
            </form>
        <% } else if (State.hpEnemyAfter <= 0) {%>
            <p>Вы выиграли</p>
            <p>Получено <%= State.randomMoney %> монет</p>
        <% } %>
        <% if (State.hpUserAfter > 0) {%>
            <form action="/newNovella/menu" method="get">
                <input type='submit' value='Следующий уровень'/>
            </form>

            <form method="get" action="/newNovella/main" align="left">
                <button type="submit">Вернуться на главную</button>
            </form>
        <% } %>
    </body>

</html>